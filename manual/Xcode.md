
We prepared a Xcode project to run on macOS machines that allows you to write C code, run the program and debug it, including triggering breakpoints and seeing the content of variables and memory. This setup works on both Intel and ARM based machines.

This guide will explain how to run and debug your assignments using XCode.


Developing and debugging with Xcode
-----------------------------------

Once Xcode is open, you can select the option to open a new project, and select the “Computer Programming Project.xcodeproj” file inside the copp-skeleton folder, or double click the same file.

![Open project in Xcode](../images/Xcode/xcode-open-project.png)

Once the project is open, the full UI of Xcode is presented. We are interested in 3 things: the project’s files, the "schemes" and the "Run" button.

![Schemes, files, and the run button](../images/Xcode/scheme-files-run.png)


### Run button

The Run button builds and runs your assignment. You can see the output of your program in the right side of the bottom view when in debug mode, just next to the variable view.

![Console output panel](../images/Xcode/panels.png)

You can also build your project with ⌘B and build and run it with ⌘R.

If you encounter errors during your build, you can find more informations in the "Issue navigator" and in the "Report navigator", accessible from the top of the left sidebar.

The "Issue navigator" usually displays all the issues that are encountered during the build process (please ignore the "Update to recommended settings"; Xcode is not used to university projects). 

![Issue navigator](../images/Xcode/issue-navigator.png)

The "Report navigator" contains a list of all the build and run operations that have been executed, together with full logs that contain all the information you may want about the build process. Usually, most build errors and warnings will directly appear in the issue navigator:

![Build issue](../images/Xcode/build-issue.png)

But in case the errors don’t show in the issue navigator but your build still fails, you can see the details of any build by clicking on the specific build that encountered the error in the report navigator, and you will see in the main window the whole logs of the build process. There is a small button with lines on it in the right side of the screen that shows additional details for each build step. Click that button on the line that contains the error to reveal the details:

![Build issue show detail button](../images/Xcode/build-issue-show-detail-button.png)

It will display a wall of text with the whole build command, and at the bottom of it it will display the actual reason why the build failed: 

![Build issue detail](../images/Xcode/build-issue-detail.png)


### Project files, test files, ijvm files and the debugger

This project contains 4 folders that we’re interested in:
- `src`
    - Contains the .c files that you will work with to implement the project. Refer to the “Introduction to C” guide for more information.
- `includes`
    - Contains the .h “header” files that you will work with to implement the project. Refer to the “Introduction to C” guide for more information.
- `tests`
    - This folder contains the tests that will run on your project to verify its functionality. Note that you can set breakpoints directly in this test code to figure out what is going on!
- `files`
    - This folder contains `.jas` and `.ijvm` files that are the IJVM programs that your program will read and execute. More specifically, the `.ijvm` files are the actually programs in hexadecimal, while the `.jas` files are meant for you to read and understand what the binary is supposed to do.

Once you click on one of these files, you'll see a text editor in the center of the Xcode window. Here, let's give a deeper look at the editor UI, specifically the line numbers.

![Line numbers](../images/Xcode/line-numbers.png)

You can click the line numbers to create a breakpoint: 

![Breakpoint](../images/Xcode/breakpoint.png)

When running your program, execution will stop in the line where you have put the breakpoint, **before execution of that line**. At that point, the UI will change to display the debug controls and the variable view.

![Debug controls](../images/Xcode/debug-controls.png)

The debug controls are a group of 4 buttons that allows you to affect how the execution will proceed from there. The options are:

*   Play button: this resumes the execution from this line.
*   Step over: this executes the instruction that is currently highlighted, and stops again before the next instruction.
*   Step in and Step out: these are used to step "inside" and "outside" of a function call. They're more typically used in higher level languages where "functions" are available. The difference between Step over, in and out is that "step over" will go to the next line in the current context, even if the current line contains a function call; "step in" will go inside the function, and stop at its first line; "step out" will execute the rest of the current function and stop again at the first line after the current function call completes. In practice, you will mostly use "Play" and "Step over". 

The "Variables view" allows us to see the current values of all variables available in the current stack frame. Xcode has sensible defaults that display the values in an appropriate format depending on their type: integer values are shown as decimal numbers, and addresses (pointers) are shown in hexadecimal by default.

Right-clicking one of these values shows a host of other very useful options, such as "View Value As", that allows you to change the representation of the value in the variable view, and "View Memory of", that shows a view where you can inspect the contents of the memory byte by byte, starting from the address that holds the content of the variable that you right-clicked.

![View value as](../images/Xcode/view-value-as.png)

![Memory view](../images/Xcode/memory-view.png)

### Adding files

To add a new file to the project you can either:
- Add a new file via Xcode.
- Add a file to the folder of the project via the finder, then add it manually to the list of compilation sources.

Of course, the first option is easier. To create a new file in a specific folder, you can right-click the folder in the Files navigator: 

![New file](../images/Xcode/new-file.png)

Then, you need to pick up what kind of file you want to add. Make sure you pick “C file” for the “macOS” platform.

![C file](../images/Xcode/c-file.png)

Then you can choose a name for the new file, and whether you want to create a header file. Typically, you want both.

![New file name](../images/Xcode/new-file-name.png)

Choose where to add the file; it should be the `src` folder. Note that Xcode will add both the `.c` file and the `.h` file to this folder; make sure you move the `.h` file to the `include` folder instead to make sure that the makefile will still be able to compile your project.

![New file location](../images/Xcode/new-file-location.png)

### Schemes

Schemes are Xcode configurations that specify which files to build and run. The project comes with a number of pre-configured schemes: one for each test suite (schemes that start with “Test”) and one for each .ijvm file to run (schemes that start with “Run”). You can select the current scheme by clicking on it, and Xcode will build and run the corresponding assignment when you click the Run button.

![Schemes](../images/Xcode/schemes.png)

