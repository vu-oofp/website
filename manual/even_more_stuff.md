# 6 - Even more stuff?! Because why not?

---
You need to pass all of the basic tests and at least 5 out of the 7 advanced tests to be eligible for bonus points. 

---

*Please list which additional features you implemented in `bonus.md` in the root directory of your project (which is included when using `make zip`). You can format your file using [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)* 

---

If you want to compile your own `jas` files to `ijvm` binaries, you can use [goJASM](https://github.com/BlackNovaTech/goJASM). You can install goJASM with `make tools`, which will install the `goJASM` binary in the `tools` directory.

## Introduction

You can get **at most** 3.5 points by implementing additional features. Keep in mind that some features are harder than others, as indicated by the number of points awarded for them.

Some of these additional features come with their own automated tests, while others don't. If a feature doesn't come with any automated tests, then testing its functionality yourself is part of the assignment.  

| Feature | Base | Additional | Total |
| ------- | ---- | ---------- | ----- |
| [Tail call](even_more_stuff.md#tail-call) | 0.5 | N/A | 0.5 |
| [Heap memory](even_more_stuff.md#heap-memory) | 0.5 | 1.5 | 2 |
| [Networking](even_more_stuff.md#networking) | 0.5 | 0.5 | 1 |
| [Snapshots](even_more_stuff.md#snapshots) | 1 | N/A | 1 |
| [Hardening](even_more_stuff.md#hardening) | 1 | N/A | 1 |
| [GUI](even_more_stuff.md#gui-graphical-user-interface) | 1 | N/A | 1 |
| [Debugger](even_more_stuff.md#debugger) | 1.5 | 1.5 | 3 |

## Tail call

| Opcode | Instruction | Args | Description |
| ------ | ----------- | ---- | ----------- |
| 0xCB   | TAILCALL    | short | Invoke a method and then return, in an optimized way.  |

Extend your emulator with [tail calls](https://en.wikipedia.org/wiki/Tail_call) by implementing the **TAILCALL** instruction, which takes the same argument as **INVOKEVIRTUAL** to indicate the method being called.

The **TAILCALL** instruction is equivalent to an **INVOKEVIRTUAL** instruction followed by an **IRETURN** instruction, with the only difference being that the caller's stack frame is cleaned up before executing the method, instead of after **IRETURN**.

You can test your implementation using **testbonustail**. To use this test, implement the `int get_call_stack_size(void)` line in `ijvm.c` (description in ijvm.h). Build and run the test with `make run_testbonustail`. Note that passing the test is not enough for getting the points, a TA will ensure that you indeed clean up the caller's stack frame before executing the method instead of after.


## Heap memory

All information in the IJVM is stored on the operand stack, however it is sometimes necessary to have persistent memory between method calls. Extend your interpreter with a [heap](https://en.wikipedia.org/wiki/Memory_management#HEAP) by implementing the three instructions below.

| Opcode | Instruction | Args | Description |
| ------ | ----------- | ---- | ----------- |
| 0xD1   | NEWARRAY    | count | Create a new array on the heap. The count is popped from the stack and replaced by an array reference that can be used to refer to the newly created array. All values in the array should initialized to zero. |
| 0xD2   | IALOAD      | index, arrayref | Push the value stored at location index in the array referenced by arrayref onto the stack |
| 0xD3   | IASTORE     | value, index, arrayref | Store value at location index in the array referenced by arrayref |


**Note:** the arguments of these instructions are placed on the stack in the order in which they appear (i.e., the last argument is at the top of the stack when the instruction is executed).

You should also implement **bounds checking** for the instructions. For example, if you use an index for the **IALOAD** instruction that goes beyond the length of the referenced array, you should print an error message and halt your interpreter.

You can test your implementation using **testbonusheap**. Use `make run_testbonusheap` to build and run the test.

#### Garbage collection

| Opcode | Instruction | Args | Description |
| ------ | ----------- | ---- | ----------- |
| 0xD4   | GC          | N/A  | Free all unused heap arrays |

Extend your heap with [garbage collection](https://en.wikipedia.org/wiki/Garbage_collection_%28computer_science%29) by freeing all **unused** heap arrays periodically, or when the program encounters the **GC** instruction. A heap array is unused if no reference to the heap array can be found anywhere in the interpreter, including other heap arrays. Your garbage collector should work with circular references: if heap array A references heap array B, and vice versa, but no other memory references array A or B, then both should be freed.

The easiest to implement is a *conservative* garbage collector, a garbage collector does not actively keep track of which values are references and which are not. If the garbage collector sees a value in memory that could be a reference (because it matches the address of some allocated memory), it assumes that it is a reference and refrains from freeing the memory in question, even if that value is actually an integer or some other kind of non-pointer data that just happens to have the same bit pattern. 
To make the chance of a false positive smaller, you can employ a strategy that ensures that array references are not small numbers (0,1,2 etc) as they are more commonly used. Conservative garbage collectors can even be used in situation which were not designed for garbage collection - for example there exists a [conservative garbage collector for C](https://en.wikipedia.org/wiki/Boehm_garbage_collector) which can also be used to find memory leaks.  

You are free to choose how to implement the garbage collector. You could, for example, keep track of existing references to arrays or implement a simple [mark-and-sweep](https://en.wikipedia.org/wiki/Tracing_garbage_collection#Na%C3%AFve_mark-and-sweep) mechanism. 

We suggest implementing the following mark-and-sweep strategy:
* *Mark:* On garbage collection traverse the entire stack (not only the frame of the current method, but also all other frames on the stack), including all local variables and arguments on the call stack, making sure to skip the link pointer, previous program counter and previous stack pointer. For any integer you encounter, if there is a corresponding heap array, mark that as used. Visit that array and do the same for all the values there.
* *Sweep:* Free all heap arrays which were not marked as used in the mark phase. 
Note that this strategy also works for circular references: if heap array A references heap array B, and vice versa, but no other memory references array A or B, then both are freed after this procedure.

The garbage collection should print a message whenever it is triggered, and include the **GC** instruction that manually triggers garbage collection. You should also include some documentation in the `bonus.md` file on how you implemented your garbage collection.

You can test your implementation using **testbonusgarbage**. To use this test, implement `bool is_heap_freed(word_t reference)` in `ijvm.c` (description in ijvm.h).  Use `make run_testbonusgarbage` to build and run the test. Note that passing the test is not enough for getting the points, a TA will also check your garbage collector.

For more of a challenge, you can also implement a *precise* garbage collector: a garbage collector which actively keeps track of which values are references and which are not - and hence cannot leak memory due to mistaking an integer for a reference. Each value must have a bit indicating wether it is a reference or not. As integers are 32-bit, there is no space within these 32 bits to store an extra bit. An easy, but wasteful solution is to store all values in 64 bits instead, using 32 bits for the value and 1 bit to indicate wether the value is a reference or not, and leave 31 bits unused (if you do this make sure you still use 32 bit arithmetic!).  

A less wasteful solution is to store the bit externally, for example all local variables/arguments together can have a *bit-map* together: some words near the local variables where the first bit indicates if the first local variable is a reference, the second bit indicates if the second local variable is a reference and so on. You also need such a bit-map for the values on the stack. Note that in the current setup you cannot specify wether an array should hold references or integers, hence arrays can mix references and integers and hence you also need a bit-map of all values in array. In the [real JVM instruction set](https://en.wikipedia.org/wiki/List_of_Java_bytecode_instructions), there are different instructions for creating an array of references and an array of primitive values, so all we then need is a single bit for the entire array. To mimic this behaviour, you can also choose to introduce three new instructions *ANEWARRAY*, *AIALOAD* and *AIASTORE*, which work exclusively for arrays of references and keep the old instructions exclusively for integers. Use `make run_testbonusprecisegarbage` to test your precise garbage collector. Use `make run_bonusaltgarbage` instead of `make runbonusgarbage` if you use *ANEWARRAY*, *AIALOAD* and *AIASTORE* for reference arrays. (Thanks to Kevin Lam for these tests!)


| Opcode | Instruction | Args | Description |
| ------ | ----------- | ---- | ----------- |
| 0xBD   | ANEWARRAY    | count | Create a new reference array on the heap. |
| 0x32   | IALOAD      | index, arrayref | Push the reference stored at location index in the array referenced by arrayref onto the stack |
| 0x53   | IASTORE     | value, index, arrayref | Store reference at location index in the array referenced by arrayref |



*Scoring*: The garbage collector can give you 1-1.5 points on top of your 0.5 points for implementing the heap. You get 1 point for a conservative garbage collector or a precise garbage collector where all values are stored in 64 bits and 1 bit is used to store whether something is a reference or not. You get 1.5 points for a precise garbage collector as described in the previous paragraph. In a precise garbage collector, you can choose to have a bitmap for arrays, or implement the extra instructions *ANEWARRAY*, *AIALOAD* and *AIASTORE*. 

## Networking

| Opcode | Instruction | Args | Description |
| ------ | ----------- | ---- | ----------- |
| 0xE1   | NETBIND     | port | Pop port from the stack and start listening for a network connection on it. Push a netref (a positive integer indicating the connection) on the stack upon success, otherwise push 0. |
| 0xE2   | NETCONNECT  | host, port | Open a network connection on the specified host (ipv4 address encoded in one word) and port. Push a netref (a positive integer indicating the connection) on the stack upon success, otherwise push 0. |
| 0xE3   | NETIN       | netref | Pop netref from the stack, receive a character from the network connection referenced by it and push the character onto the stack |
| 0xE4   | NETOUT      | char, netref | Pop netref and a word from the stack and send that character to the network connection referenced by netref |
| 0xE5   | NETCLOSE    | netref | Pop netref from the stack and close the network connection referenced by it |

**Note:** the arguments of these instructions are placed on the stack in the order in which they appear (i.e., the last argument is at the top of the stack when the instruction is executed). 

As seen in the course *Computer Networks*, most modern applications depend on networked access to other devices. Extend your emulator to support **one** network connection. You are allowed to use the [GNU C library](https://www.gnu.org/software/libc/manual/html_node/Sockets.html) (see **sys/socket.h** and **arpa/inet.h**) to implement the networking. See this [article](https://www.geeksforgeeks.org/socket-programming-cc/) for an overview of important socket programming concepts and example code. Since in this assignment there is only one network connection, netref can always be 1. 

There are currently no automated tests for this feature, however we have provided you with some IJVM binaries you can use to test your implementation. You can find these binaries in **files/bonus**. The provided binaries are one-sided: the server is provided by **test_netbind.ijvm** and as the client is provided by **test_netconnect.ijvm**.

Currently, the server and client do the same thing: recieve two bytes and send them back in reverse. You can hence not connect the server to the client. It is up to you to act as the corresponding counterpart (i.e., client or server) in both cases. To achieve this, use a networking utility, such as [netcat](https://en.wikipedia.org/wiki/Netcat), or write a simple script in Python (or any other language of your choice) that handles the other end of the connection. You can also adjust and compile the server or client such that they can be connected to each other.  

### Multiple connections

Extend your emulator to support **multiple** network connections. Use the `netref` argument as a network connection identifier. Modify the **NETBIND** and **NETCONNECT** instructions to push a meaningfull `netref` onto the stack instead and modify the **NETIN** and **NETOUT** instructions to make use of the `netref` to distinguish between different network connections.

There are currently no automated tests for this feature, however it is sufficient to modify the provided **test_netbind.jas** and **test_netconnect.jas** files to support multiple connections. Keep in mind that the modified JAS files have to be recompiled using [goJASM](https://github.com/BlackNovaTech/goJASM).

## Snapshots

One benefit of virtual machines is that the machine is (surprise) virtualised. This allows for convenient features, such as migration and snapshots. Extend your emulator with snapshots by saving the state of your IJVM instance to a file when receiving a `SIGINT` signal (<kbd>Ctrl</kbd> + <kbd>C</kbd>). To handle `SIGINT`, you need to implement a **signal handler**.

One thing of note is that when you trigger your signal handler your program could have not yet fully completed a step, thus you need to account for this case by letting the current step fully complete and *only then* serializing the state such that, upon restoring, the machine executes with a valid intial state. **This must be implemented to receive the bonus point**

You need to serialise the state of your machine. Store all specifics like the program counter, local variables and other memory contents in the snapshot. It is important to note that you should not save `FILE` pointers when saving input and output files, as files can be temporary.

You then have to be able to start up a new IJVM instance (add a command-line option to your emulator) from the serialised file, continuing at the state where you saved the snapshot. It is up to you to decide the format of the serialised file. 

There are currently no automated tests for this feature, however it is sufficient to be able to recover from a `SIGINT` when running **testadvanced6**.

Hint: Signal handlers return you to back to the place where you executed from with any modifications from the signal handler function applied to your program. To read up more on how to properly work with signal handlers checkout [this resource](https://wiki.sei.cmu.edu/confluence/display/c/SIG31-C.+Do+not+access+shared+objects+in+signal+handlers)

## Hardening
Harden your emulator. Make sure your program does not crash due to memory errors (i.e., segmentation faults) even when given an invalid binary.

We do not expect you to catch all possible errors in your program, mainly due to limited time. Our standard policy is you should make a best-effort to check for errors. 

An example of an easily exploitable part is the offsets used in the binary. If the index value of the **ISTORE** instruction is not checked, this could be used to divert the control flow of the program.

Make sure to check for overflows and underflows and check the sizes of values that you read. Make sure to use tools, such as [fuzzers](https://en.wikipedia.org/wiki/Fuzzing) and [sanitisers](https://clang.llvm.org/docs/AddressSanitizer.html), to find bugs in your implementation. See this [guide](https://fuzzing-project.org/tutorial1.html) on fuzzing to get started.

Document the hardening process (in the README) and quantify the improvement (e.g., using [code coverage](https://en.wikipedia.org/wiki/Code_coverage)). Also describe your development process (i.e., how you found the errors in your code, what inputs were able to crash your program, etc.) and how your changes might affect performance (if applicable).

Your program must compile with `make pedantic` and pass `testsanitizers`.

**Requirements:**

1. Set up a fuzzer, such as [AFL](https://github.com/google/AFL), to find bugs and let it run for about an hour
2. Analyse how much of your code gets [covered](https://clang.llvm.org/docs/SourceBasedCodeCoverage.html) by our automated tests and or by the test cases generated by the fuzzer
3. Write a short report in a `bonus.md`` file on what you did to mitigate bugs and errors. The main challenge of this additional feature lies with analysing what the attack surface is, what you can guarantee about your implementation and how to mitigate possible exploitation.

**Note**: hardening your emulator is the most open-ended of all the additional features. You have a lot of freedom in how you approach this. Keep in mind that you might have to set up a bunch of different tools and learn some new concepts.

## GUI (Graphical user interface)

Extend your emulator with a GUI, which allows the user to select a binary to be executed, reset the state of the IJVM instance and enter input and read output (see [WebIJVM or IJVMore](introduction_to_the_ijvm.md#web-based-implementations) for inspiration). There are many libraries that allow you to implement a GUI in C, such as [GTK](https://www.gtk.org/), [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) and [raygui](https://github.com/raysan5/raygui). Remember to document (in the README) the libraries you used and how to install them.

Put your GUI in `src/gui.c`, you can build this using `make gui`. Make a file called `gui_libs.txt` in the root folder of your project (you are not allowed to change the Makefile, as codegrade replaces it with the original one). In this you can put addditional compiler command line arguments (for example `-lgtk-4` for linking with gtk-4), these will be included when running `make gui`. Make sure your file does not end in an enter.

Possible features (pick at least 3):

* Allow the user to select a binary to be executed
* Allow the user to reset the state of the IJVM instance and restart the execution
* Allow the user to enter input in a text field and display the output
* Allow the user to select a file to be as input instead of waiting for keystrokes
* Allow the user to save the IJVM output to a file of choice
* Display information, such as the program counter and stack contents
* Make your GUI portable, so that it works on other operating systems

## Debugger

Extend your emulator with an interactive memory debugger with roughly the same functionality as [GDB](useful_tools_and_debugging.md#gdb). The program should start on the command line and give the user a prompt. Your debugger should have the following commands:

* `help`: print help on how to use the debugger
* `file <binary>`: load the specified binary
* `run`: run until the next breakpoint is encountered. If the program has already started, stop and start again.
* `input <file>`: set the IJVM input to contain contents of specified file
* `break <addr>`: set a breakpoint for instruction at address addr
* `step`: perform one instruction
* `continue`: continue executing until the next breakpoint
* `info`: show the local stack (in hexadecimal) and variables of the current frame
* `backtrace`: show a [call stack](https://en.wikipedia.org/wiki/Call_stack) of all frames (i.e., in which order methods have called each other and with what arguments)

Create a new program, **debugger**, that reads commands from the command-line, and starts up a new IJVM instance that can be debugged when the run command is executed. You are allowed to use the **readline** library for command line parsing.

Put your debugger in `src/debugger.c`, you can build this using `make debugger`. Make a file called `debugger_libs.txt` in the root folder of your project (you are not allowed to change the Makefile, as codegrade replaces it with the original one). In this you can put addditional compiler command line arguments (for example `-lgtk-4` for linking with gtk-4), these will be included when running `make debugger`. Make sure your file does not end in an enter.

Look at how the available commands map to the interface specified in **ijvm.h** for an idea on how to start implementing a debugger.

### Debug symbols

Extend your debugger with debug symbol handling. The [goJASM](https://github.com/BlackNovaTech/goJASM) assembler can add debug symbols to a binary. Debug symbols allow you to break the execution at a certain method or at a certain label. Extend the `break` command such that besides an address, you could also provide a label or a method to break at (e.g., `break myfunc`, should break the execution when the method `myfunc` starts executing).

The debug symbols for methods and labels can be found in the third and fourth block of the binary respectively. The format of these sections is as follows:

```console
debugblock = [entry]*
entry = <32-bit instruction address> <symbol name> <null terminator>
symbol name = [char]+char = <any ASCII letter>
null terminator = ’\0’
```

For example, suppose you have a debug section with an entry with address `0x1337` and symbol name `l33tfunction`. If you execute `break l33tfunction`, you should break at address `0x1337`. The debug symbols for labels should be handled in the same manner. Since different methods can have labels with the same identifier, the assembler automatically prepends the method name to the identifier of labels (e.g., label `L1` in `myfunc` becomes `myfunc#L1`).
