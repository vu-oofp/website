
### Developing and debugging with VScode
----------------------------------- 
When you open VSCode it should look something like this 
![[../images/VSCode/main-window.png]]

Once the project is open, the full UI of VScode is presented. We are interested in 2 things: the terminal and the files.

![Project window](../images/VSCode/project-window.png)

Once you have created a new terminal you are presented with three different windows
- Problems : Indicates issues currently being detected in your C program 
- Debug Console : The console which runs when you debug your program 
- Terminal : The terminal where you can execute the make commands to run your program

![Terminal](../images/VSCode/terminal-with-information.png)


### The debugger and program files

This project contains 4 folders that we’re interested in:
- `src`
    - Contains the .c files that you will work with to implement the project. Refer to the “Introduction to C” guide for more information.
- `includes`
    - Contains the .h “header” files that you will work with to implement the project. Refer to the “Introduction to C” guide for more information.
- `tests`
    - This folder contains the tests that will run on your project to verify its functionality. Note that you can set breakpoints directly in this test code to figure out what is going on!
- `files`
    - This folder contains `.jas` and `.ijvm` files that are the IJVM programs that your program will read and execute. More specifically, the `.ijvm` files are the actually programs in hexadecimal, while the `.jas` files are meant for you to read and understand what the binary is supposed to do.

Once you click on one of these files, you'll see a text editor in the center of the VScode window. Here, let's give a deeper look at the editor UI, specifically the line numbers.

To create a breakpoint, click on a spot to the *left* of the corresponding line number, this should create a little red dot.

![Breakpoint](../images/VSCode/breakpoints-labeled.png)

Once you have created the breakpoints at the desired positions you want to go to the **Run and Debug** section, then press the little green play button, this should open up a dropdown menu containing all the ijvm files.

![Debug selection](../images/VSCode/selecting-file-to-debug.png)

Now when you select a program you wish to debug you will be presented with alot of information. So lets break down what everything means.
- *Variables* 
	This includes all the variables currently in your local scope. If you look next to the particular variable you should see a small file icon, this file icon allows you to view the memory at address of that variable if you click on it.
	Note : For the memory viewer you will be prompted to install the `Hex Editor` extension, if this prompt does not occur you should go install this extension beforehand.
- *Watch* 
	The watch section allows you to focus in on specific variables, you can right click on a variable to add it to watch.
- *Breakpoints* 
	This section allows you to enable and disable breakpoints without having to add and remove them.
- *Debug console*
	This shows you debug information in the more classical gdb view.

Then there are the main  debug controls which are a group of 6 buttons that allows you to affect how the execution will proceed from there. The options are:

*   Play button: this resumes the execution from this line.
*   Step over: this executes the instruction that is currently highlighted, and stops again before the next instruction.
*   Step in and Step out: these are used to step "inside" and "outside" of a function call. They're more typically used in higher level languages where "functions" are available. The difference between Step over, in and out is that "step over" will go to the next line in the current context, even if the current line contains a function call; "step in" will go inside the function, and stop at its first line; "step out" will execute the rest of the current function and stop again at the first line after the current function call completes. In practice, you will mostly use "Play" and "Step over". 
- Restart : Restarts the program within the debugger, so useful if you want to start from the first breakpoint again.
- Stop : Stops the execution of the program ( and the current debugging session )

![Debugger view](../images/VSCode/active-debugger-view.png)

Here is a closeup of the buttons in the order described 
![Debugger buttons](../images/VSCode/debugger-buttons.png)
And here is a closeup of the memory viewer for variables 
![Variables](../images/VSCode/memory-viewer.png)

### The Command Palette 
-----
Certain tasks can be quickly executed via something called the command palette. To bring it up press `CTRL + SHIFT + P`  then the main action you can do from here is building the different tests or running certain groups of tests.

- **Building specific tests**
	1. Type `Run Build Task` and press enter on the menu item 
	2. Select the test you want to build and press enter on the test
	
![Command pallete build](../images/VSCode/command-palette-build.png)
![Command pallete build](../images/VSCode/command-palette-build2.png)

- **Running groups of tests**
	1. Type `Run Test Task` and select the menu item 
	2. Select the group of tests that you want to run
	
![Command pallete test](../images/VSCode/command-palette-test.png)
![Command pallete test](../images/VSCode/command-palette-test2.png)

For other other commands such as building and running specific tests take a look at the `Makefile` and the guide.
