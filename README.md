# Computer Programming Project 2024

*Do not post solution of this assignment online!* This is considered plagiarism by the exam board (this holds for all courses at the VU unless specified otherwise). If you store your repository at github, make it private!!

## Goal of this course

The aim of this course is to give you experience with programming a larger project. This larger project involves some of the concepts you became familiar with during the first year of the Bachelor Computer Science, allowing you to put in to practice what you have learned.

## The course in a nutshell

In this course you will implement an [interpreter](https://en.wikipedia.org/wiki/Interpreter_%28computing%29) which executes [IJVM](https://en.wikipedia.org/wiki/IJVM) bytecode, in the programming language C. The assignment is split into several smaller parts that build up to the final deliverable.

## Before starting on the project

First follow the [Installation instructions](manual/framework.md).

And read the following:
*  [VScode guide](manual/VSCode.md)(Windows/Linux) or [Xcode guide](manual/Xcode.md)
*  [Introduction to C](manual/introduction_to_c.md) 
*  [C - Style guidelines](manual/code_style.md)
*  [Introduction to the IJVM](manual/introduction_to_the_ijvm.md)
*  [Skeleton usage](manual/skeleton.md)

## The project

The project is split into five main chapters. Each chapter builds on the previous chapters and introduces new tasks. We have ranked the difficulty of the chapters to help you better manage your time. Of course, these are just indications, your experience might be different.

| Chapter | Difficulty |
| ------- | ---------- |
| [Binaries, dreaded Binaries!](manual/binaries_dreaded_binaries.md) | ★★★☆☆ |
| [Stack up!](manual/stack_up!.md) | ★★★★☆ |
| [Controlling the Flow: the GOTO solution!](manual/controlling_the_flow.md) | ★★☆☆☆ |
| [Local variables: Artisan and Organic!](manual/local_variables.md) | ★★★★☆ |
| [Call yourself a method!](manual/call_yourself_a_method.md) | ★★★★★ |
| (Optional) [Even more stuff](manual/even_more_stuff.md) | |



## Grading:


* Your program **must** pass all the **basic** tests. There are a total of 5 basic tests and each basic test is worth 0.6 points. (3 points)
* Your program **must** pass at least 5 of the **advanced** tests. There are a total of 8 advanced tests and each advanced test is worth 0.5 points. (4 points)
* You must pass all JAS assignments (each before its deadline) on canvas to pass the course.
* You must have passed in the IJVM Basics assignment to pass the course.
* If you pass all the basic tests and at least 5 of the advanced tests, your program will be graded on style and general impression. (0.5 point)
* You can achieve a higher grade by implementing additional functionality (3.5 points). **Note:** you are only eligible for these points if you pass at least 5 out of 8 the advanced tests.
* Naturally, your final grade is **capped at a 10**.


 You need at least **5.5 points** to pass the course. **Note:** It is *NOT* possible to pass by style points. 

 All submitted code, including comments, must be in English.

 You can compute your grade (excluding bonus and style points) by doing `make grade` (after following the installation instructions).

## Oral evaluation

If you pass all the basic tests and at least 4 of the advanced tests, you can schedule an oral evaluation. The purpose of the oral evaluation is to confirm that you have submitted your own work. We do this by discussing the inner details and history of your code. You **must** pass the oral evaluation to pass the course. The oral evaluation will take place on  Friday the 30th of June or on Monday the 3rd of July, unless arranged otherwise with your TA. The oral evaluation does not affect your final grade.

## Deadline

Please submit your solution before **Thurday 27th of June 23:59** and make sure that it passes the automated tests on CodeGrade. This deadline is **strict**. You can submit your code as many times as you want, so please do not wait until the last minute for your first submission.

## Handing in 

To hand in your assignment use `make zip` which creates a file called `dist.zip` in your project directory. Handing this in using codegrade. You can drag this zip from the vscode list of files to codegrade if you position the windows side by side. 

Of course, you can also find this file using your file explorer. On windows, the file is (probably) at Linux -> /home/<username>/copp-skeleton/dist.zip (path : `\\wsl$/home/<username>/copp-skeleton/dist.zip`


* If you ever get the situation where a test fails on codegrade but succeeds on your system, run `make testsanitizers`. This will use the Clang sanitizers, which instrument your code to detect behaviour which is not defined in the C standard such as array indexing out of bounds, use of uninitialized variables or reliance on an undefined order. * We recommend you always use the sanititizers before submitting to codegrade.


Your program has to compile and work on  Ubuntu 20.04 LTS  x86-64 with Clang. Check the output of our submission system to make sure that your program compiles on the target environment.


## Plagiarism and a note on doing the assignment yourself

Do not copy code from each other or from the internet!

This is an individual programming assignment, which means that you have to program the assignment by yourself. It is not sufficient to understand the program you hand in, you **must** have programmed it yourself. 

It is allowed to discuss the assignments and solutions broadly.

You are allowed help from a TA or a fellow student that already solved it if you are stuck. It is then:

* OK to show your (not-working) code to them and discuss what is wrong.
* NOT OK to look at their (working) code.
The reason here is that the former leads to you understanding what is wrong with your own code, while the latter likely leads to you submitting the same solution as your fellow student.

As soon as you are unstuck, please continue working individually. It is not allowed to program together with a friend/parent/tutor, as it is then impossible to see what can be attributed to you.

We automatically check your code for similarities with the submissions of other students and known solutions. 

**Do not post your solutions online!** This is not allowed as sharing solutions is also considered fraud by the exam committee (fraud is defined as any action that makes fair assessment impossible). 

Please refer to the [Teaching and Examination Regulations](https://assets.vu.nl/f55d3574-6c6d-0002-e968-70643a2e365a/2a38a57a-77d9-4aed-ae05-727ec3647ab3/TER_B_Computer%20Science_2019-2020.pdf) and [The Examination Board’s Rules and Guidelines](https://science.vu.nl/en/Images/rules-and-guidelines-examination-board-2019-2020_24_tcm296-934031.pdf) for additional information.
